\version "2.18.2"

melody = \relative c' {
    fis e8 g~ g fis d4 |
    a2 r |
    fis'4 e8 g~ g fis d4 |
    fis2 e |
    e4 dis8 fis~ fis e cis4 |
    e d cis d |
    b8 cis4 e8~ e d4 a8~ |
    a2 r |

    fis'4 e8 g~ g fis d4 |
    a2 r |
    fis'4 e8 g~ g fis d4 |
    fis2 e |
    e4 dis8 fis~ fis e cis4 |
    e d cis d |
    b8 cis4 e8~ e d4 fis8~ |
    fis4. fis8 g4 b4 |
    a1 |
    r4. fis8 g4 b |
    a8 e4 g8~ g fis fis4~ |
    fis2 \tuplet 3/2 {fis4 g a} |
    cis b a r8 d, |
    cis'4 b a r8 d, |
    fis e4 d8~ d8 b d4 |
    e r8 fis g4 b |
    a1 |
    r4. fis8 g4 b |
    a8 e4 g8~ g fis fis4~ |
    fis2 \tuplet 3/2 {fis4 g a} |
    cis b a r8 d, |
    cis'4 d a r8 d, |
    fis4 e d e |
    g fis d cis |
    d1 |
    r |
    \key f \major
    a'4 g8 bes~ bes a f4 |
    c2. r4 |
    a' g8 bes~ bes a f4 |
    a2 g2 |
    g4 fis8 a~ a g e4 |
    g f! e f |
    d8 e4 g8~ g f4 a8~ |
    a4 r8 a bes4 d |
    c1
}

middle = \relative c {
    \tiny
    \hideNotes
    r1 |
    r1 |
    r1 |
    \unHideNotes
    d'4 g, bes g |
    \hideNotes
    r1 |
    r1 |
    r1 |
    \unHideNotes
    g4. fis8~ fis4 a |
    \hideNotes
    r1 |
    r1 |
    r1 |
    \unHideNotes
    d4 g, bes g |
    \hideNotes
    r1 |
    r1 |
    r1 |
    r1 |
    r1 |
    r1 |
    r1 |
    r1 |
    r1 |
    r1 |
    r1 |
}

bass = \relative c {
    d,2 a' |
    fis' a, |
    d, a' |
    b cis |
    <g g' b!> fis4 ais |
    <b g' b>2 <a g' b> |
    g r |
    <d a'> r4 <cis' e> |
    d,2 a' |
    fis' a, |
    d, a' |
    b cis |
    <g g' b!> fis4 ais |
    <b g' b>2 <a g' b> |
    g r |
    d' r |
    cis r |
    d r |
    <a cis'>4. <ais bes' cis>8~ <ais bes' cis>4 <d a' e'>~ |
    <d a' e'>2 r |
    <g fis'> <fis d'> |
    <g fis'> <fis dis'> |
    <b, fis' d'> <b a'>4 <bes aes'> |
    r2 <a b' d> |
    <cis b' e> r2 |
    r1 |
    r1 |
    r1 |
    r1 |
    r1 |
    r1 |
    r1 |
    r1 |
    r1 |
    r1 |
    r1 |
    r1 |
    <d bes' f'>2 <e bes' c> |
    <bes bes' cis>4 r <a bes' cis>2 |
    <d a' d> <c a' d>
}


music = << \melody \\ \bass \\ \\ \middle >>

\score {
    \context Staff <<
        \set Staff.instrumentName = "Guitar"
        \set Staff.midiInstrument = "acoustic guitar (nylon)"
        { \clef "treble_8" \key d \major \tempo 4 = 95 \time 4/4 \music}
    >>
    \layout {}
    \midi {}
}
